**Pruebas de modelo entrenado**

Según el Git oficial de Detectron2, se comenta que existen 3 familias de modelos:

<img src="Salmons/imagenes/Imagen20.png" title="" alt="Imagen20.png" width="563">

Donde la Familia de FPN está basada en ResNet+FPN. Se decide usar esta con el modelo ResNet-50 para entrenar.

Se realiza un entrenamiento de 16 minutos.

<img title="" src="Salmons/imagenes/Imagen17.png" alt="Imagen17.png" width="499">

Luego se prueba su desempeño con el Dataset de COCO obteniedo el siguiente AP:

<img src="Salmons/imagenes/Imagen16-fpn50.png" title="" alt="Imagen16-fpn50.png" width="508">

Se observan los resultado con una imagen de salmones.

<img src="Salmons/imagenes/peces-ev.png" title="" alt="peces-ev.png" width="508">

Vemos que lo máximo logrado es de un 80% de probabilidad y lo mínimo de un 63% siedo todos salmones.





Se investiga en el Git Oficial de DETECTRON2, donde explican cuáles son los mejores modelos.

<img title="" src="Salmons/imagenes/Imagen21.png" alt="Imagen21.png" width="526">

Se decide ocupar el modelo com mejor AP,  X101-FPN.


<img src="Salmons/imagenes/Imagen18.png" title="" alt="Imagen18.png" width="508">



Realizando un entrenamiento de 18 minutos, para el modelo X101-FPN.

Obteniendo los siguientes resultados en la pueba de evaluación de COCO.

<img src="Salmons/imagenes/Imagen15-x101.png" title="" alt="Imagen15-x101.png" width="498">

Existiendo una mejora respecto al modelo anterior. 
Se espera con un entrenamiento más extendido y con un dataset más amplio, lograr un mejor resultado.

<img src="Salmons/imagenes/prueba1-2.jpg" title="" alt="prueba1-2.jpg" width="443">

Vemos que lo máximo logrado es de un 96% de probabilidad y lo mínimo de un 90% siedo todos salmones.


