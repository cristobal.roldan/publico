**Readme**

<img src="Salmons/imagenes/prueba1-2.jpg" title="" alt="prueba1-2.jpg" width="350">

Link google colab final para segmentación de salmones, aplicable para imágenes.

Incluye manual de usuario y de instalación.

[Google Colab](https://colab.research.google.com/drive/1XPwXbUeeNYELa59lOZrIOmG2QyeZE34X?usp=sharing)

Link a los resultados; Imágenes, y videos.

[Salmons/RESULTADOS · main · Cristóbal Eduardo Roldán Solís / Publico · GitLab](https://gitlab.com/cristobal.roldan/publico/-/tree/main/Salmons/RESULTADOS)

Link a Google Colab para entrenamiento 

[Google Colab](https://colab.research.google.com/drive/1J8sH83mOsuQpb2UNlgBQb3xXYG1Xj1h8?usp=sharing)


