**Elección de Detectron2.**



<img src="/Salmons/imagenes/Imagen3.png" title="" alt="Imagen3.png" width="509">

Según Paperswithcode, actualmente le mejor modelo para instance segmentation es EVA.

Al investigar en el repositorio oficial de EVA, encontramos que este “Es muy nuevo”

<img src="/Salmons/imagenes/Imagen4.png" title="" alt="Imagen4.png" width="529">

Posee poca información, pocas estrellas, forks y branch e issues.

Al final de la sección de Instance segmentation, aparece que está basado en DETECTRON2

<img src="/Salmons/imagenes/Imagen5.png" title="" alt="Imagen5.png" width="540">



Al revisar el github, se encuentra que Detectron2, dice ser la “biblioteca de próxima generación de Facebook IA research” , por lo que se decide instalar y probar.

Detectron2 posee una mejor documentación y al ser un desarrollo de Facebook  inspira más confiabilidad y respaldo.

<img src="/Salmons/imagenes/Imagen6.png" title="" alt="Imagen6.png" width="562">



Se crea un Google Colab , se instalan los pre-requisitos y el las bibliotecas necesarias.

Se realiza la prueba con una imagen de peces.

<img src="/Salmons/imagenes/Imagen8.png" title="" alt="Imagen8.png" width="396">

Resultando la siguiente segmentación de instancias.

<img src="/Salmons/imagenes/Imagen7.png" title="" alt="Imagen7.png" width="504">

Vemos que las máscaras son bastante aceptables, pero no acertó en la etiqueta de identificación, por lo que se concluye que el modelo no incluye peces dentro de su set de clasificaciones.


