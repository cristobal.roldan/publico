Como se vio anteriormente, el modelo no posee la clasificación de Salmón, ni de peces, por lo que no reconocía estos elementos:

<img title="" src="/Salmons/imagenes/Imagen7.png" alt="Imagen7.png" width="620">

Observando que los detecta como pájaros y aviones.

Se propone entrenar el modelo, para el reconocimiento de salmones. 
Para esto se debió modificar el Dataset de salmones, entregado por Felipe Leiva, para que poseyera el mismo formato y estructura que el requerido para e l entrenamiento de Detectron2.



<img src="/Salmons/imagenes/Imagen9.png" title="" alt="Imagen9.png" width="513">

Una vez adaptado el formato, se comprimió y subió al GitLab personal - Público.

<img title="" src="/Salmons/imagenes/Imagen10.png" alt="Imagen10.png" width="520">

[Salmons · main · Cristóbal Eduardo Roldán Solís / Publico · GitLab] (https://gitlab.com/cristobal.roldan/publico/-/tree/main/Salmons)

Luego se modificó el código que estructura los datos para estar acorde al dataset. 

<img src="/Salmons/imagenes/Imagen11.png" title="" alt="Imagen11.png" width="543">

Luego se prueba el dataset, verificando con tres imágenes al azar, que obtenga sus etiquetas y datos según el Json asociado.

<img src="/Salmons/imagenes/Imagen12.png" title="" alt="Imagen12.png" width="556">

Vemos que el dataset está cargado correctamente.

Se procede a entrenar el modelo, por medio del dataset.

<img title="" src="/Salmons/imagenes/Imagen13.png" alt="Imagen13.png" width="584">

Luego de unos 12 minutos, se obtiene un LOSS de 0.6 (bastante malo, pero es un entrenamiento de prueba)

<img title="" src="/Salmons/imagenes/Imagen15.png" alt="Imagen15.png" width="625">

Probamos el nuevo modelo:

<img src="/Salmons/imagenes/Imagen14.png" title="" alt="Imagen14.png" width="585">

Observamos que ya comienza a detectar Salmones.

Se realizan nuevos entrenamientos e iteraciónes.


